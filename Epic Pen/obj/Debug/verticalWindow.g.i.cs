﻿#pragma checksum "..\..\verticalWindow.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "8F85F01C79376F53786EE468354000A9"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Epic_Pen {
    
    
    /// <summary>
    /// VerticalWindow
    /// </summary>
    public partial class VerticalWindow : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 56 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid windowGrid;
        
        #line default
        #line hidden
        
        
        #line 58 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas toolGrid;
        
        #line default
        #line hidden
        
        
        #line 59 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button cursorButton;
        
        #line default
        #line hidden
        
        
        #line 71 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas brushSizeCanvas;
        
        #line default
        #line hidden
        
        
        #line 85 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button eraserButton;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Canvas Colors;
        
        #line default
        #line hidden
        
        
        #line 89 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Border selectedColourBorder;
        
        #line default
        #line hidden
        
        
        #line 91 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Blue;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Red;
        
        #line default
        #line hidden
        
        
        #line 97 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Image Green;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.CheckBox hideInkCheckBox;
        
        #line default
        #line hidden
        
        
        #line 108 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button penButton;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\verticalWindow.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button highlighterButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/epicpen;component/verticalwindow.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\verticalWindow.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.windowGrid = ((System.Windows.Controls.Grid)(target));
            
            #line 56 "..\..\verticalWindow.xaml"
            this.windowGrid.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.toolGrid = ((System.Windows.Controls.Canvas)(target));
            return;
            case 3:
            this.cursorButton = ((System.Windows.Controls.Button)(target));
            
            #line 59 "..\..\verticalWindow.xaml"
            this.cursorButton.Click += new System.Windows.RoutedEventHandler(this.cursorButton_Click);
            
            #line default
            #line hidden
            return;
            case 4:
            this.brushSizeCanvas = ((System.Windows.Controls.Canvas)(target));
            return;
            case 5:
            
            #line 72 "..\..\verticalWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.penSizeButton_MouseDown);
            
            #line default
            #line hidden
            return;
            case 6:
            
            #line 75 "..\..\verticalWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.penSizeButton_MouseDown);
            
            #line default
            #line hidden
            return;
            case 7:
            
            #line 78 "..\..\verticalWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.penSizeButton_MouseDown);
            
            #line default
            #line hidden
            return;
            case 8:
            
            #line 81 "..\..\verticalWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.penSizeButton_MouseDown);
            
            #line default
            #line hidden
            return;
            case 9:
            this.eraserButton = ((System.Windows.Controls.Button)(target));
            
            #line 85 "..\..\verticalWindow.xaml"
            this.eraserButton.Click += new System.Windows.RoutedEventHandler(this.eraserButton_Click);
            
            #line default
            #line hidden
            return;
            case 10:
            this.Colors = ((System.Windows.Controls.Canvas)(target));
            return;
            case 11:
            this.selectedColourBorder = ((System.Windows.Controls.Border)(target));
            return;
            case 12:
            this.Blue = ((System.Windows.Controls.Image)(target));
            
            #line 91 "..\..\verticalWindow.xaml"
            this.Blue.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Border_MouseDown);
            
            #line default
            #line hidden
            return;
            case 13:
            this.Red = ((System.Windows.Controls.Image)(target));
            
            #line 94 "..\..\verticalWindow.xaml"
            this.Red.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Border_MouseDown);
            
            #line default
            #line hidden
            return;
            case 14:
            this.Green = ((System.Windows.Controls.Image)(target));
            
            #line 97 "..\..\verticalWindow.xaml"
            this.Green.MouseDown += new System.Windows.Input.MouseButtonEventHandler(this.Border_MouseDown);
            
            #line default
            #line hidden
            return;
            case 15:
            
            #line 106 "..\..\verticalWindow.xaml"
            ((System.Windows.Controls.Button)(target)).Click += new System.Windows.RoutedEventHandler(this.Button_Click);
            
            #line default
            #line hidden
            return;
            case 16:
            this.hideInkCheckBox = ((System.Windows.Controls.CheckBox)(target));
            return;
            case 17:
            this.penButton = ((System.Windows.Controls.Button)(target));
            
            #line 108 "..\..\verticalWindow.xaml"
            this.penButton.Click += new System.Windows.RoutedEventHandler(this.penButton_Click);
            
            #line default
            #line hidden
            return;
            case 18:
            this.highlighterButton = ((System.Windows.Controls.Button)(target));
            
            #line 111 "..\..\verticalWindow.xaml"
            this.highlighterButton.Click += new System.Windows.RoutedEventHandler(this.highlighterButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

