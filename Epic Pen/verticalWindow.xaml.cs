﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Epic_Pen
{
    /// <summary>
    /// Interaction logic for verticalWindow.xaml
    /// </summary>
    public partial class VerticalWindow : Window
    {

        InkCanvas inkCanvas;
        public VerticalWindow()
        {
            InitializeComponent();
        }

        public void setInkCanvas(InkCanvas _inkCanvas)
        {
            inkCanvas = _inkCanvas;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(0);
        }

        private void MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed)
                DragMove();
        }

        private void Border_MouseDown(object sender, MouseButtonEventArgs e)
        {
            String cl = ((Image)sender).Name;
            selectedColourBorder.Background = (SolidColorBrush)new BrushConverter().ConvertFromString(cl);
            //inkCanvas.DefaultDrawingAttributes.Color = ((SolidColorBrush)((Border)sender).Background).Color;
            inkCanvas.DefaultDrawingAttributes.Color = (Color)ColorConverter.ConvertFromString(cl);
        }

        public event EventHandler CloseButtonClick;

        private void Grid_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            onCloseButtonClick();
        }

        void onCloseButtonClick()
        {
            if (CloseButtonClick != null)
                CloseButtonClick.Invoke(new object(), new EventArgs());
        }

        private void resetAllToolBackgrounds()
        {
            foreach (Button i in toolGrid.Children.OfType<Button>())
                i.Style = defaultButtonStyle;
        }

        public void cursorButton_Click(object sender, RoutedEventArgs e)
        {
            resetAllToolBackgrounds();
            cursorButton.Style = (Style)FindResource("highlightedButtonStyle");
        }
        public void penButton_Click(object sender, RoutedEventArgs e)
        {
            inkCanvas.Cursor = Cursors.Pen;
            inkCanvas.EditingMode = InkCanvasEditingMode.Ink;
            inkCanvas.DefaultDrawingAttributes.IsHighlighter = false;
            setBrushSize();
            resetAllToolBackgrounds();
            penButton.Style = (Style)FindResource("highlightedButtonStyle");
        }

        public void highlighterButton_Click(object sender, RoutedEventArgs e)
        {
            inkCanvas.Cursor = Cursors.Pen;
            inkCanvas.EditingMode = InkCanvasEditingMode.Ink;
            inkCanvas.DefaultDrawingAttributes.IsHighlighter = true;
            setBrushSize();
            resetAllToolBackgrounds();
            highlighterButton.Style = (Style)FindResource("highlightedButtonStyle");

        }
        
        public void eraserButton_Click(object sender, RoutedEventArgs e)
        {
            inkCanvas.Cursor = Cursors.Cross;
            inkCanvas.EditingMode = InkCanvasEditingMode.EraseByStroke;
            setBrushSize();
            resetAllToolBackgrounds();
            eraserButton.Style = (Style)FindResource("highlightedButtonStyle");   
        }
        
        public void eraseAllButton_Click(object sender, RoutedEventArgs e)
        {
            inkCanvas.Strokes.Clear();
        }
        double penSize=3;
        private void penSizeButton_MouseDown(object sender, RoutedEventArgs e)
        {
            penSize = ((Ellipse)((Button)sender).Content).Width;
            setBrushSize();

            foreach (Button i in brushSizeCanvas.Children)
                i.Style = defaultButtonStyle;
            ((Button)sender).Style = (Style)FindResource("highlightedButtonStyle");   
        }

        private void setBrushSize()
        {
            if (inkCanvas.Cursor == Cursors.Cross)
            {
                inkCanvas.DefaultDrawingAttributes.Width = penSize * 5;
                inkCanvas.DefaultDrawingAttributes.Height = penSize * 5;
            }
            else
            {
                inkCanvas.DefaultDrawingAttributes.Width = penSize;
                inkCanvas.DefaultDrawingAttributes.Height = penSize;
            }
        }

        private void clickThroughCheckBox_Checked(object sender, RoutedEventArgs e)
        {

            //if ((bool)hideInkCheckBox.IsChecked)
            //{
            //    //toolsDockPanel.Height = 0;
            //    DoubleAnimation doubleAnimation = new DoubleAnimation();
            //    doubleAnimation.From = toolsDockPanelDefaultHeight;
            //    doubleAnimation.To = 0;
            //    doubleAnimation.Duration = new Duration(new TimeSpan(0,0,0,0,200));
            //    ExponentialEase expoEase = new ExponentialEase();
            //    expoEase.Exponent = 7;
            //    doubleAnimation.EasingFunction = expoEase;
            //    //Storyboard.SetTargetName(doubleAnimation, toolsDockPanel.Name);
            //    Storyboard.SetTarget(doubleAnimation, toolsDockPanel);
            //    Rectangle rect = new Rectangle();
            //    Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(DockPanel.HeightProperty));
            //    Storyboard storyboard = new Storyboard();
            //    storyboard.Children.Add(doubleAnimation);
            //    storyboard.Begin();
            //}
            //else
            //{
            //    //toolsDockPanel.Height = double.NaN;
            //    DoubleAnimation doubleAnimation = new DoubleAnimation();
            //    doubleAnimation.From = 0;
            //    doubleAnimation.To = toolsDockPanelDefaultHeight;
            //    doubleAnimation.Duration = new Duration(new TimeSpan(0, 0, 0,0, 200));
            //    ExponentialEase expoEase = new ExponentialEase();
            //    expoEase.Exponent = 7;
            //    doubleAnimation.EasingFunction = expoEase;
            //    //Storyboard.SetTargetName(doubleAnimation, toolsDockPanel.Name);
            //    Storyboard.SetTarget(doubleAnimation, toolsDockPanel);
            //    Rectangle rect = new Rectangle();
            //    Storyboard.SetTargetProperty(doubleAnimation, new PropertyPath(DockPanel.HeightProperty));
            //    Storyboard storyboard = new Storyboard();
            //    storyboard.Children.Add(doubleAnimation);
            //    storyboard.Begin();
            //}

        }

        Style defaultButtonStyle;
        double toolsDockPanelDefaultHeight;
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            //toolsdockpanel.height = toolsdockpanel.actualheight;
            //toolsdockpaneldefaultheight = toolsdockpanel.height;
            //height = actualheight;
            //sizetocontent = system.windows.sizetocontent.manual;
            //defaultbuttonstyle = eraseallbutton.style;
            var desktopWorkingArea = System.Windows.SystemParameters.WorkArea;
            this.Left = desktopWorkingArea.Right - this.Width;
            this.Top = desktopWorkingArea.Bottom - this.Height;
        }
    }
}
